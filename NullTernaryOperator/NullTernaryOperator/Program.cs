﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullTernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 1;  
            int y = x ?? -1;  
            Console.Write("Response 1: " + y + "\n"); 
            Console.ReadKey();
        } 
    }
}
