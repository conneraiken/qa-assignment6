﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace NullTernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 0;
            int y = x ?? -1;

            if (y == 0)
                Console.WriteLine("pass");
            else
                Console.WriteLine("fail");
             
        }
    }
}