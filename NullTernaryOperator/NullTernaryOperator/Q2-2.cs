﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace NullTernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = Int32.MaxValue;
            int y = x ?? -1;

            if (y == 2147483647)
                Console.WriteLine("pass");
            else
                Console.WriteLine("fail");
 
        }
    }
}
