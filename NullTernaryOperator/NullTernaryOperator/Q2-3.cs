﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace NullTernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = Int32.MinValue;
            int y = x ?? -1;

            if (y == -2147483648)
                Console.WriteLine("pass");
            else
                Console.WriteLine("fail");
 
        }
    }
}
