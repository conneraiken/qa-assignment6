﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NullTernaryOperator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace NullTernaryOperator.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void Test1()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-1.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-1.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-1.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail();
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
        [TestMethod()]
        public void Test2()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-2.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-2.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-2.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail();
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
        [TestMethod()]
        public void Test3()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-3.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-3.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-3.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail();
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
        [TestMethod()]
        public void Test4()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-4.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-4.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-4.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail();
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
        [TestMethod()]
        public void Test5()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-5.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-5.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-5.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail();
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
        [TestMethod()]
        public void FaliureTest()
        {

            const string strCmdText = "/out:C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-6.exe C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-6.cs";
            const string strCmdText2 = "C:\\Users\\conne\\Dropbox\\QA-Final-Assignment\\NullTernaryOperator\\NullTernaryOperator\\Q2-6.exe";
            {
                Process prc = new Process();
                prc.StartInfo.FileName = "C:\\WINDOWS\\Microsoft.NET\\Framework\\v3.5\\csc.exe";
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.Arguments = strCmdText;
                try
                {
                    prc.Start();
                    prc.WaitForExit();
                    prc.Close();
                }
                catch
                {
                    Assert.Fail();
                }
            }

            {

                Process prc = new Process();
                prc.StartInfo.FileName = strCmdText2;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardOutput = true;

                try
                {
                    prc.Start();

                    // Synchronously read the standard output of the spawned process. 
                    StreamReader reader = prc.StandardOutput;
                    string output = reader.ReadToEnd();

                    // Write the redirected output to this application's window.
                    Console.WriteLine(output);
                    Assert.AreEqual("pass", output.Trim());
                }
                catch
                {
                    Assert.Fail("Could not start the program, maybe it was not compiled?");
                }
                prc.WaitForExit();
                prc.Close();
            }

        }
    }
}